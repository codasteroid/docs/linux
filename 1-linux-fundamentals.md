## Linux Fundamentals - Part 1

Embark on the journey of learning the fundamentals of Linux. Learn to run some of the first essential commands on an interactive terminal.

[Reference](https://tryhackme.com/r/room/linuxfundamentalspart1)

### A Bit of Background on Linux

The first release of a Linux operating system was in 1991. Linux is considerably much more lightweight. For example, Ubuntu server can run on systems with only 512MB of RAM.

Linux powers things such as:

* Website that we visit.
* Car entertainment / control panels.
* Point of Sale (PoS) systems such as checkout tills and registers in shops.
* Critical infrastructures like traffic light controllers or industrial sensors.

Similar to Windows versions (7, 8, Vista, etc), there are many different distributions of Linux (Ubuntu, Debian, Kali, etc).

### Understanding the Basics

* **Kernel**: Core component of the operating system. It manages system resources such as CPU, memory, devices, and filesystems. It provides an abstraction layer between hardware and software, allowing software to interact with the hardware without needing to know specific details of each device.

* **Shell**: Command-line interface (CLI) that interprets user commands and executes them. It acts as an intermediary between the user and the operating system. Shells provide features like command execution, scripting capabilities, variable manipulation, and control structures (loops and conditionals). Common shells in Linux include:

  *   **Bash (Bourne Again Shell)**: The default shell for most Linux distributions.
  *   **Zsh (Z Shell)**: A powerful and customizable shell with advanced features like tab completion and theme support.
  *   **Fish (Friendly Interactive Shell)**: A user-friendly shell with auto-suggestions and syntax highlighting.
  
* **Terminal**: Software application that provides users with a text-based interface to interact with the shell. Terminals can be graphical or text-based, depending on the environment. Graphical terminals are integrated into Desktop environments like GNOME terminal or KDE Konsole, while text-based terminals are available in virtual consoles accessed through key combinations like Ctrl+Alt+F1.


### Interacting with the Filesystem

| Command | Full Name             |
|---------|-----------------------|
| ls      | list files in the current directory               |
| cd      | change the current directory      |
| cat     | output the content of file           |
| pwd     | print the full path to the current working directory |

### Searching for Files

* **Find**: Search for specific files.

```bash
rochdikhalid@lat3450:~ find -name passwords.txt
./folder1/passwords.txt
rochdikhalid@lat3450:~
```

```bash
rochdikhalid@lat3450:~ find -name *.txt
./folder1/passwords.txt
./Documents/todo.txt
rochdikhalid@lat3450:~
```

* **Grep**: Search for a specific value within a file.

```bash
rochdikhalid@lat3450:~ grep "81.143.211.90" access.log
81.143.211.90 - - [25/Mar/2021:11:17 + 0000] "GET / HTTP/1.1" 200 417 "-" "Mozilla/5.0 (Linux; Android 7.0; Moto G(4))"
rochdikhalid@lat3450:~
```

### Introduction to Shell Operators

| Symbol/Operator | Description                                                                                                    |
|-----------------|----------------------------------------------------------------------------------------------------------------|
| &               | Run commands in the background of the terminal.                                   |
| &&              | Combine multiple commands together in one line of the terminal.                   |
| >               | This operator is a redirector - meaning that we can take the output from a command and direct it elsewhere.   |
| >>              | This operator does the same function of the > operator but appends the output rather than replacing (meaning nothing is overwritten).

**Some Examples**:

```bash
rochdikhalid@lat3450:~ echo howdy > welcome
howdy
rochdikhalid@lat3450:~ echo howdy >> welcome
howdy
howdy
```

